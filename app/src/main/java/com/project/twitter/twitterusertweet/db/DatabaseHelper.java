package com.project.twitter.twitterusertweet.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;


import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.project.twitter.twitterusertweet.model.TTweet;
import com.project.twitter.twitterusertweet.model.User;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
	// name of the database file for your application -- change to something
	// appropriate for your app
	private static final String DATABASE_NAME = "saarath.sqlite";

	// any time you make changes to your database objects, you may have to
	// increase the database version
	private static final int DATABASE_VERSION = 22;
	private ConnectionSource mConnectionSource;
	private static final String TAG = "Database Helper";

	// the DAO object we use to access the SimpleData table
	private Dao<User, Integer> userDao = null;
	private Dao<TTweet, Integer> tweetsDao= null;

	// context is used because we write to shared preference connector about the
	// db sync status
	private Context context;

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase database,
			ConnectionSource connectionSource) {
	
		try {
			//mConnectionSource=connectionSource;
			// purpose of these tables are documented in dbschema
			TableUtils.createTable(connectionSource, User.class);
			TableUtils.createTable(connectionSource, TTweet.class);

						
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} catch (java.sql.SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

	}


	// Dao methods for all the tables
	public Dao<User, Integer> getUserDao() {
		if (null == this.userDao) {
			try {
				this.userDao = getDao(User.class);
			} catch (java.sql.SQLException e) {
				e.printStackTrace();
			}
		}
		return this.userDao;
	}



	public Dao<TTweet, Integer> getTweetDao() {
		if (null == this.tweetsDao) {
			try {
				this.tweetsDao = getDao(TTweet.class);
			} catch (java.sql.SQLException e) {
				e.printStackTrace();
			}
		}
		return this.tweetsDao;
	}



}
