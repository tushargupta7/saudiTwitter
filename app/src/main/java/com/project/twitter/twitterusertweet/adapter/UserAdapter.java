package com.project.twitter.twitterusertweet.adapter;

/**
 * Created by tushar on 11/6/16.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.project.twitter.twitterusertweet.R;
import com.project.twitter.twitterusertweet.model.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;



public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {
    private static ArrayList<User> userEntries;
    private static Context context;
    private static int currentPage = 1;
    public OnclickListener mListener;

    public UserAdapter(ArrayList<User> ProductEntry, Context context,OnclickListener listener) {
        this.userEntries = ProductEntry;
        this.context = context;
        mListener=listener;
    }

    @Override
    public UserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_card_view, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(UserAdapter.ViewHolder holder, final int position) {
        User userEntry = userEntries.get(position);
        holder.followerCount.setText("Followers: "+String.valueOf(userEntry.getNumFollower()));
        holder.tweetCount.setText("Tweets: "+String.valueOf(userEntry.getNumTweet()));
        holder.userName.setText("Name: " +userEntry.getDisplayName());
        Picasso.with(context).load(userEntry.getProfilePicUrl()).into(holder.productImage);
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClicked(position, userEntry.getName());
            }
        });
    }

    private String getFormattedTime(Date createdAt) { //displays the difference of current time and
        Calendar c = Calendar.getInstance();             //time of tweet
        Date d = c.getTime();
        long millis = d.getTime() - createdAt.getTime();
        long sec = millis / 1000;
        int min = (int) sec / 60;
        int Hours = min / 60;
        int days = Hours / 24;
        min = min % 60;
        if (days > 0) {
            return days + " day ago";
        } else if (Hours > 0) {
            return Hours + " hour ago";
        }
        if (min == 0) {
            return "just now";
        } else {
            return min + " min ago";
        }

    }

    @Override
    public int getItemCount() {
        return userEntries.size();
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void incCurrentPage() {
        currentPage++;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView productImage;
        public TextView userName;
        public TextView followerCount;
        public TextView tweetCount;
        public LinearLayout card;


        public ViewHolder(final View view) {
            super(view);
            productImage = (ImageView) view.findViewById(R.id.img);
            userName = (TextView) view.findViewById(R.id.name);
            followerCount = (TextView) view.findViewById(R.id.followerCount);
            tweetCount = (TextView) view.findViewById(R.id.tweetCount);
            card=(LinearLayout)view.findViewById(R.id.card);
        }
    }

    public interface OnclickListener{
       void onItemClicked(int position, String name);
    }
}
