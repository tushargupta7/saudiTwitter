package com.project.twitter.twitterusertweet.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;


import com.project.twitter.twitterusertweet.R;
import com.project.twitter.twitterusertweet.db.DatabaseManager;
import com.project.twitter.twitterusertweet.model.TTweet;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.UserTimeline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TweetFragment.OnTweetFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TweetFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TweetFragment extends ListFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnTweetFragmentInteractionListener mListener;
    private ProgressDialog dialog;

    public TweetFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TweetFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TweetFragment newInstance(String param1, String param2) {
        TweetFragment fragment = new TweetFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

            System.out.println("Param 1: "+mParam1);
            System.out.println("Param 2:screenName "+ mParam2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tweet, container, false);

    }

    @Override
    public void onResume() {
        super.onResume();
        GetTweetDataAsync tweetDataAsync=new GetTweetDataAsync();
        tweetDataAsync.execute(mParam1);
    }


    private class GetTweetDataAsync extends AsyncTask<String, Void, List<Tweet>> {
        List<Tweet> tweetList;
        @Override
        protected List<Tweet> doInBackground(String... name) {
            TwitterCore twitterCore=TwitterCore.getInstance();
            try {
                return  twitterCore.getApiClient().getStatusesService().userTimeline(null,
                        name[0], 10, null, null, false, false, null,
                        true).execute().body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Tweet> s) {
            super.onPostExecute(s);
            if(s==null || s.size()==0){
                loadFromDatabase();
                return;
            }
            ArrayList<TTweet> tweets=new ArrayList<>();
            for (Tweet tw:s
                    ) {
                TTweet tTweet=new TTweet();
                tTweet.setName(tw.user.screenName);
                tTweet.setTweetMsg(tw.text);
                tweets.add(tTweet);
            }
            DatabaseManager.getInstance(getContext()).addTTweet(tweets);
            loadTweetsResponse();
            dialog.dismiss();
        }

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(getActivity(), "Loading...", "Please wait...", true);
            dialog.setCancelable(true);
        }
    }

    private void loadTweetsResponse() {
        UserTimeline userTimeline = new UserTimeline.Builder()
                .screenName(mParam1).maxItemsPerRequest(10).includeRetweets(true)
                .build();
        final TweetTimelineListAdapter adapter = new TweetTimelineListAdapter.Builder(getActivity())
                .setTimeline(userTimeline)
                .build();
        setListAdapter(adapter);
    }

    private void loadFromDatabase() {
        List<TTweet> tweetList= DatabaseManager.getInstance(getContext()).getTweetsById(mParam1);
        ArrayList<String> values=new ArrayList<>();
        if(tweetList.size()==0){
            Snackbar.make(getView(), "Check network conectivity", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
        for (TTweet tweet:tweetList
                ) {
            values.add(tweet.getTweetMsg());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);
        dialog.dismiss();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTweetFragmentInteractionListener) {
            mListener = (OnTweetFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnTweetFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



}
