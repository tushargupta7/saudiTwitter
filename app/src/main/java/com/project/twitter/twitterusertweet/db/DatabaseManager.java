package com.project.twitter.twitterusertweet.db;

import android.content.Context;

import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.table.TableUtils;
import com.project.twitter.twitterusertweet.model.TTweet;
import com.project.twitter.twitterusertweet.model.User;

import java.sql.SQLException;
import java.util.List;

//TODO: error handling strategies
public class DatabaseManager {

	private static final String TAG = "Database Manager";
	static private DatabaseManager instance;
	private static Context context;


	static public void init(Context ctx) {
		if (null == instance) {
			instance = new DatabaseManager(ctx.getApplicationContext());
			context = ctx.getApplicationContext();
		}
	}

	static public DatabaseManager getInstance(Context ctx) {
		if(null==instance){
			instance = new DatabaseManager(ctx.getApplicationContext());
		}
		context = ctx;
		return instance;
	}

	private DatabaseHelper helper;

	private DatabaseManager(Context ctx) {
		helper = new DatabaseHelper(ctx);
	}

	private DatabaseHelper getHelper() {
		return helper;
	}

	public List<User> getAllUser() {
		List<User> users = null;
		try {
			users = getHelper().getUserDao().queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}

	public List<TTweet> getTweetsById(String name) {
		List<TTweet> tweetList= null;
		try {
			tweetList = getHelper().getTweetDao().queryForEq("name",name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tweetList;
	}

	public void addUser(List<User> l) {
		try {
			TableUtils.clearTable(getHelper().getConnectionSource(), User.class);
			for (User user:l
				 ) {
				getHelper().getUserDao().createOrUpdate(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	public void addTTweet(List<TTweet> l) {
		try {
			DeleteBuilder<TTweet,Integer> deleteBuilder=getHelper().getTweetDao().deleteBuilder();
			deleteBuilder.where().eq("name", l.get(0).getName());
			deleteBuilder.delete();
			for (TTweet tw:l
				 ) {
				getHelper().getTweetDao().createOrUpdate(tw);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
