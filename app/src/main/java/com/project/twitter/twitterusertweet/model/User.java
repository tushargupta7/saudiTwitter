package com.project.twitter.twitterusertweet.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by tushar on 27/3/18.
 */
@DatabaseTable
public class User {
    @DatabaseField(generatedId = true)
    private int id_d;
    @DatabaseField
    private String name;
    @DatabaseField
    private String displayName;
    @DatabaseField
    private String profilePicUrl;
    @DatabaseField
    private long numFollower;
    @DatabaseField
    private long numTweet;
    @DatabaseField
    private long createdDate;
    @DatabaseField
    private Date latestTweet;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public long getNumFollower() {
        return numFollower;
    }

    public void setNumFollower(long numFollower) {
        this.numFollower = numFollower;
    }

    public long getNumTweet() {
        return numTweet;
    }

    public void setNumTweet(long numTweet) {
        this.numTweet = numTweet;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public void setLatestTweet(Date latestTweet) {
        this.latestTweet = latestTweet;
    }
    public Date getLatestTweet() {
        return latestTweet;
    }
}
