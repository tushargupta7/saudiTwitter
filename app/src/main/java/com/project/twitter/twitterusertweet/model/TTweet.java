package com.project.twitter.twitterusertweet.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by tushar on 28/3/18.
 */
@DatabaseTable
public class TTweet {

    @DatabaseField(generatedId = true)
    private int id_d;
    @DatabaseField
    private String name;
    @DatabaseField
    private String tweetMsg;

    public String getTweetMsg() {
        return tweetMsg;
    }

    public void setTweetMsg(String tweetMsg) {
        this.tweetMsg = tweetMsg;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
