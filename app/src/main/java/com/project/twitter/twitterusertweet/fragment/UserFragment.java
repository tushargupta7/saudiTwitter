package com.project.twitter.twitterusertweet.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.project.twitter.twitterusertweet.R;
import com.project.twitter.twitterusertweet.adapter.UserAdapter;
import com.project.twitter.twitterusertweet.db.DatabaseManager;
import com.project.twitter.twitterusertweet.model.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserFragment extends Fragment implements View.OnClickListener, UserAdapter.OnclickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String base64= "ZTkxcnhKSW44QzVOMVV5Z1A5NURxYjk4azpNYnNrZkROY1k5VEcxZHlHbkpXUEFUcWg4MDlLeDZ2R2ZlY1J1RkxVZEFBd0ZtUE1RSQ==";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private boolean flag = false;
    private RecyclerView recyclerView;
    private UserAdapter userAdapter = null;
    private ArrayList<User> userList;
    private OnFragmentInteractionListener mListener;
    private ProgressDialog dialog;
    private ArrayList<User> users;



    public UserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserFragment newInstance(String param1, String param2) {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        userList = new ArrayList<>();
    }

    public void sortByLatestTweet() {
        if(users.size()==0){
            Snackbar.make(getView(), "Please Check Network Connection", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }
        Collections.sort(users, new Comparator<User>() {
            public int compare(User o1, User o2) {
                if (o1.getLatestTweet() == null || o2.getLatestTweet() == null)
                    return 0;
                return o1.getLatestTweet().compareTo(o2.getLatestTweet());
            }
        });
        Collections.reverse(users);
        Snackbar.make(getView(), "Sorted by recently active", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
        createCardViews(users);
    }

    public void sortByFollowerCount() {
        if(users.size()==0){
            Snackbar.make(getView(), "Please Check Network Connection", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }
        Collections.sort(users, new Comparator<User>() {
            public int compare(User o1, User o2) {
                if (o1.getNumFollower() == 0 || o2.getNumFollower() == 0)
                    return 0;
                return (o1.getNumFollower()>o2.getNumFollower()?1:-1);
            }
        });

        Snackbar.make(getView(), "Sorted by follower count", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
        Collections.reverse(users);
        createCardViews(users);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event


    private void initRecylerView() {
        recyclerView = (RecyclerView) getView().findViewById(R.id.user_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    private void createCardViews(List<User> users) {
        userList.clear();
        userList.addAll(users);
        userAdapter = new UserAdapter(userList, getActivity(), this);
        recyclerView.setAdapter(userAdapter);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FloatingActionButton myFab = (FloatingActionButton) view.findViewById(R.id.sortTweet);
        myFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!flag){
                    sortByLatestTweet();
                }
                else
                    sortByFollowerCount();

                flag = !flag;
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onItemClicked(int position, String name) {
        if (mListener != null) {
            mListener.onClick(position, name);
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onClick(int position, String name);
    }

    public void startUserDataAsync(){
        getUserData getData = new getUserData();
        getData.execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        initRecylerView();
        startUserDataAsync();
    }


    private class getUserData extends AsyncTask<Void, Void, String> {

        OkHttpClient client = new OkHttpClient();


        @Override
        protected String doInBackground(Void... params) {

            String bearerToken = null;

            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, "grant_type=client_credentials");
            Request requestToken = new Request.Builder()
                    .url("https://api.twitter.com/oauth2/token")
                    .post(body)
                    .addHeader("authorization", "Basic "+ base64)
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .build();

            try {
                Response response = client.newCall(requestToken).execute();
                JSONObject token = new JSONObject(response.body().string());
                bearerToken = token.getString("access_token");
            } catch (Exception e) {
                e.printStackTrace();
            }

            Request request = new Request.Builder()
                    .url("https://api.twitter.com/1.1/users/lookup.json?screen_name=BarackObama%2Cjustinbieber%2Ckatyperry%2Crihanna%2Ctaylorswift13")
                    .get()
                    .addHeader("authorization", "Bearer "+bearerToken)
                    .build();
            try {
                Response response = client.newCall(request).execute();
                return response.body().string();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            super.onPostExecute(res);
            try {
                users = new ArrayList<>();
                JSONArray userData = new JSONArray(res);
                for (int i = 0; i < userData.length(); i++) {
                    JSONObject user = userData.getJSONObject(i);
                    User user1 = new User();
                    user1.setName(user.getString("screen_name"));
                    user1.setDisplayName(user.getString("name"));
                    user1.setProfilePicUrl((user.getString("profile_image_url_https")).replace("normal", "200x200"));
                    user1.setNumFollower(Long.parseLong(user.getString("followers_count")));
                    user1.setNumTweet(Long.parseLong(user.getString("statuses_count")));
                    user1.setLatestTweet(new Date(user.getJSONObject("status").getString("created_at")));
                    users.add(user1);
                }
                DatabaseManager.getInstance(getContext()).addUser(users);
            } catch (Exception e) {
                users= (ArrayList<User>) DatabaseManager.getInstance(getContext()).getAllUser();
                //createCardViews(users);
                e.printStackTrace();
            }
            sortByFollowerCount();
            dialog.dismiss();
        }

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(getActivity(), "Loading...", "Please wait...", true);
            dialog.setCancelable(true);
        }

    }
}