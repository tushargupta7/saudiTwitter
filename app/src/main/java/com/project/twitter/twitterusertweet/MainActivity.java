package com.project.twitter.twitterusertweet;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.project.twitter.twitterusertweet.fragment.TweetFragment;
import com.project.twitter.twitterusertweet.fragment.UserFragment;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

public class MainActivity extends AppCompatActivity implements TweetFragment.OnTweetFragmentInteractionListener, UserFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Twitter.initialize(this);
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig("DTxZxF2PVRdSftjPm9wBOlnaH", "qVHWJ1OkOwp80anutDwG7jiSpxF7iRxB0rtsNcU1PqoQ1iGSvQ"))
                .debug(true)
                .build();
        Twitter.initialize(config);
        loadFragment("User");
        //UserTimeline userTimeline = new UserTimeline.Builder().screenName("twitterdev").build();

    }

    private void loadFragment(String tag, String...name) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (tag){
            case "User":
                UserFragment fragment = UserFragment.newInstance("tushargupta71","");
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.commit();
                break;
            case "Tweet":
                TweetFragment tweetFragment = TweetFragment.newInstance(name[0],"");
                fragmentTransaction.replace(R.id.fragment_container, tweetFragment);
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public void onClick(int position, String name) {
        loadFragment("Tweet", name);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        loadFragment("User");
    }
}
